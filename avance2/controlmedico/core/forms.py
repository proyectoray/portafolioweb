from django import forms
from .models import Puesto, Empleado, Alergia

class PuestoForm(forms.ModelForm):
    class Meta:
        model = Puesto
        fields = ['nombre', 'descripcion']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre del puesto'}),
            'descripcion': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Descripción del puesto'}),
        }

class EmpleadoForm(forms.ModelForm):
    class Meta:
        model = Empleado
        fields = ['nombrePila', 'apPat', 'apMat', 'fnacimiento', 'puesto']
        widgets = {
            'nombrePila': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre del empleado'}),
            'apPat': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido paterno'}),
            'apMat': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido materno'}),
            'fnacimiento': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'puesto': forms.Select(attrs={'class': 'form-control'}),
        }

class AlergiaForm(forms.ModelForm):
    class Meta:
        model = Alergia
        fields = ['nombre', 'empleado']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre de la alergia'}),
            'empleado': forms.Select(attrs={'class': 'form-control'}),
        }

