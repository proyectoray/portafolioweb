from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import timedelta
# Create your models here.

class Puesto(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField(max_length=100)
    
    def __str__(self):
        return self.nombre

class Empleado(models.Model):
    nombrePila = models.CharField(max_length=20,null=True)  
    apPat = models.CharField(max_length=20,null=True)  
    apMat = models.CharField(max_length=20,null=True)  
    fnacimiento = models.DateField()
    puesto = models.ForeignKey(Puesto, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nombrePila

class Alergia(models.Model):
    nombre = models.CharField(max_length=100)
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nombre
